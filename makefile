.DEFAULT_GOAL := all

all:

clean:
	cd examples/python; make clean

config:
	git config -l

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs373.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add examples
	git add makefile
	git add notes
	git commit -m "another commit"
	git push
	git status

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	@rsync -r -t -u -v --delete            \
    --include "Script.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.py"                   \
    --include "Assertions.py"              \
    --include "UnitTests1.py"              \
    --include "UnitTests2.py"              \
    --include "UnitTests3.py"              \
    --include "Coverage1.py"               \
    --include "Coverage2.py"               \
    --include "Coverage3.py"               \
    --include "IsPrime.py"                 \
    --include "Exceptions.py"              \
    --include "Types.py"                   \
    --include "Operators.py"               \
    --include "FactorialT.py"              \
    --include "Reduce.py"                  \
    --include "ReduceT.py"                 \
    --include "Iteration.py"               \
    --include "Comprehensions.py"          \
    --include "Map.py"                     \
    --include "MapT.py"                    \
    --include "Yield.py"                   \
    --include "Range.py"                   \
    --include "RangeT.py"                  \
    --include "Iterables.py"               \
    --include "Functions.py"               \
    --include "Decorators.py"              \
    --include "DecoratorPattern.py"        \
    --include "DecoratorPatternT.py"       \
    --include "FunctionDefaults.py"        \
    --include "FunctionKeywords.py"        \
    --include "FunctionUnpacking.py"       \
    --include "FunctionTuple.py"           \
    --include "FunctionDict.py"            \
    --include "Select.py"                  \
    --include "SelectT.py"                 \
    --include "Project.py"                 \
    --include "ProjectT.py"                \
    --include "CrossJoin.py"               \
    --include "CrossJoinT.py"              \
    --include "ThetaJoin.py"               \
    --include "ThetaJoinT.py"              \
    --include "NaturalJoin.py"             \
    --include "NaturalJoinT.py"            \
    --include "RegExps.py"                 \
    --exclude "*"                          \
    ../../examples/python/ examples/python/
	@rsync -r -t -u -v --delete            \
    --include "ShowDatabases.sql"          \
    --include "ShowDatabases.out"          \
    --include "ShowEngines.sql"            \
    --include "ShowEngines.out"            \
    --include "CreateDatabase.sql"         \
    --include "CreateDatabase.out"         \
    --include "CreateTables.sql"           \
    --include "CreateTables.out"           \
    --include "Insert.sql"                 \
    --include "Insert.out"                 \
    --include "Select.sql"                 \
    --include "Select.out"                 \
    --include "Like.sql"                   \
    --include "Like.out"                   \
    --include "SelectT.sql"                \
    --include "SelectT.out"                \
    --include "Join.sql"                   \
    --include "Join.out"                   \
    --exclude "*"                          \
    ../../examples/sql/ examples/sql/
	@rsync -r -t -u -v --delete            \
    --include "StrategyPattern1.java"      \
    --include "StrategyPattern2.java"      \
    --include "StrategyPattern3.java"      \
    --include "StrategyPattern4.java"      \
    --include "StrategyPattern5.java"      \
    --include "StrategyPattern6.java"      \
    --include "SingletonPattern.java"      \
    --include "StrategyPattern7.java"      \
    --include "Reflection.java"            \
    --include "StrategyPattern8.java"      \
    --exclude "*"                          \
    ../../examples/java/ examples/java/
	@rsync -r -t -u -v --delete            \
    --include "SAC.uml"                    \
    --include "SAC.svg"                    \
    --include "StrategyPattern1.uml"       \
    --include "StrategyPattern1.svg"       \
    --include "StrategyPattern2.uml"       \
    --include "StrategyPattern2.svg"       \
    --include "StrategyPattern3.uml"       \
    --include "StrategyPattern3.svg"       \
    --include "StrategyPattern4.uml"       \
    --include "StrategyPattern4.svg"       \
    --include "StrategyPattern5.uml"       \
    --include "StrategyPattern5.svg"       \
    --include "StrategyPattern6.uml"       \
    --include "StrategyPattern6.svg"       \
    --include "StrategyPattern7.uml"       \
    --include "StrategyPattern7.svg"       \
    --include "StrategyPattern8.uml"       \
    --include "StrategyPattern8.svg"       \
    --exclude "*"                          \
    ../../examples/uml/ examples/uml/
	@rsync -r -t -u -v --delete            \
    --include "Collatz.py"                 \
    --include "RunCollatz.in.ctd"          \
    --include "RunCollatz.in"              \
    --include "RunCollatz.out.ctd"         \
    --include "RunCollatz.out"             \
    --include "RunCollatz.py"              \
    --include "TestCollatz.py"             \
    --exclude "*"                          \
    ../../projects/python/collatz/ projects/collatz/
