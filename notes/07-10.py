# -----------
# Wed, 10 Jul
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
customer / developer

back left
water we doing /  who is paying

back right
how's my air / engage climate

front
food for thought / degree for me
"""

"""
customer / developer

back left
who is paying / how's my air

back right
engage climate / food for thought

front
degree for me / water we doing
"""
