# -----------
# Wed, 26 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

for v in a :
    print(v)

"""
what does a have to be
a has to be iterable
"""

p = iter(a) # a.__iter__()

try :
    while True :
        v = next(p) # p.__next__()
        print(v)
except StopIteration :
    pass

a = [2, 3, 4]
print(type(a)) # list

p = iter(a)
print(type(p)) # list iterator

"""
reduce (<binay function>, <iterable>, <value>)
higher-order function

<value> <binay function> <first element>
    <result> <binay function> <second element>
        <result> <binay function> <third element>
"""

for v in a : # a has to be iterable
    ...

for u, v in a : # a has to be iterable over iterables of length 2
    ...

a = [2, 3, 4]
print(type(a)) # list

p = iter(a)
print(type(p)  # list iterator

print(a is p)  # False

q = iter(p)
print(p is q)  # True

a = [2, 3, 4, ...]

# call f() on the first three elements
# call g() on the rest

p = iter(a)
f(next(p))
f(next(p))
f(next(p))

for v in p : # the rest of the elements
    g(v)
