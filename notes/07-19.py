# -----------
# Fri, 19 Jul
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
movie table

title year director genre
"shane" 1953 "george stevens" "western"
"star wars" 1977 "george lucas" "sci-fi"
...
"""

"""
director table

director_id name
1 "george stevens"
2 "george lucas"

movie table

title year director_id genre
"shane" 1953 1 "western"
"star wars" 1977 2 "sci-fi"
...
"""

"""
RE vs SQL

.* is equivalent to %
.  is equivalent to _
"""
