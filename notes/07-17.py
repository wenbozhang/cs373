# -----------
# Wed, 17 Jul
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
theta_join(a relation, another relation, a predicate)
"""

"""
natrual_join(a relation, another relation)
"""

"""
regular expressions
very useful to do find and replace
"""

"""
*: 0 or more
+: 1 or more
?: 0 or 1
"""
