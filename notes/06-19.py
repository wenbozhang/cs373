# -----------
# Wed, 19 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
list, like ArrayList
front-loaded array
cost of adding to the beg: O(n)
cost of adding to the mid: O(n)
cost of adding to the end: amortized O(1)
cost of indexing: O(1)
mutable
change the content and the size
no inherent order
heterogenous
"""

class jay :
    ...

x = jay();

print(type(x))    # jay
print(type(jay))  # type
print(type(type)) # type

"""
tuple is like list, but immutable
"""

x = (2, 3, 4)
print(len(x)) # 3
y = (5, x, 6)
print(y)      # (5, (2, 3, 4), 6)

x = [2, 3, 4]
print(len(x)) # 3
y = (5, x, 6)
print(y)      # (5, [2, 3, 4], 6)
x[1] = 7
print(x)      # [2, 7, 4]
print(y)      # (5, [2, 7, 4], 6)

"""
set, hash
mutable, change length, can not change content
unordered, no indexing
unique values
elements have to be hashable
the mutables are not hashable
"""

"""
frozenset, like tuple
hash
elements have to be hashable
the mutables are not hashable
"""
