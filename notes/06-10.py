# -----------
# Mon, 10 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
broken tests can hide broken code
3716

1. run the code, confirm success
2. identify and fix the tests
3. run the code, confirm failure
4. identify and fix the code
5. run the code, confirm success
"""

"""
ways in which you could indicate end of input

1. end of file
2. sentinel at the end
3. tell me up front
"""

"""
read eval print loop (REPL)
"""

"""
1. write some tests, only a few, manually
2. write the stupidest solution
3. write many tests, mechanically
4. share those tests
5. refactor the solution to make it elegant, modular, clear
make it easily testable
"""

"""
5 16 8 4 2 1
create a cache, an array, for old answers
store lots more than the numbers in the range
store values as I read
one possibility is an array of size 1,000,000
lazy cache
"""

"""
eager cache
store values before any read
one possibility is an array of size 1,000,000
"""

"""
meta cache
don't cache cycle lengths
cache max cycle lengths, instead
1,1000
1001, 2000
2001, 3000
...

max_cycle_length(1500, 3500)
compute 1500 to 2000
look up 2001 to 3000
compute 3001 to 3500
"""

"""
unit tests, also called white box tests
acceptance tests, also called black box tests
""""

"""
the kernel
    Collatz.py

run harness
    RunCollatz.py

test harness
    TestCollatz.py
"""
