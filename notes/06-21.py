# -----------
# Fri, 21 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
deque
doubly-linked list
adding to front/back: O(1)
add to middle: O(1)
a lot like list in behavior
"""

"""
Java has a closed object model
    every instance of a class has the same data
    an instance doesn't change data over time
Python has open object model
    different instance can have different data
    an instance can have different data over time
    changes to instances can even happen outside of the class
"""

# Java

class A {
    void f (...) {
        g(...);}}

# Python

def g (...) :
    ...

class A :
    def g (...) :
        ...
    def f (...) :
        g(...)

"""
a higher-order function is a function that takes or returns a function
"""

reduce (..., +)   # no
reduce (..., lambda x, y : x + y)
reduce (..., add)

def f (...) :
    ...
    def g (...)
        ...
    ...
    return g

"""
Python duck typing
"""

class A :
    pass

x = A()
y = A()
print(x == y)
print(x[0])   # no

"""
list's +  requires another list
list's += will take any iterable
"""

i = 2
j = 3
i += j
# vs.
i = i + j

"""
list does not conform to that
mutables do not conform

tuple does conform
immutables do
"""

"""
tuple's +  requires another tuple
tuple's += requires another tuple
"""
