# -----------
# Mon, 15 Jul
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
SQL is a declarative language
Python/C/Java/C++ are procedural languages
"""

"""
dealing relational databases
relational algebra
"""

"""
algebras
    elements
    operations
"""

"""
arithemetic
    numbers
    +, -, *, / ...
"""

"""
some algebras are closed or open
"""

"""
integers are closed on +, -, *
they're open on division
"""

"""
relational algebra is closed
elements: tables or relations
operations: select, project, cross join, theta join, natural join
"""

"""
movie table

title, year, director, genre
"shane", 1953, "george stevens", "western"
"star wars", 1977, "george lucas", "western"
"""

"""
select (a relation, a unary predicate)
select (movie, movies made after 1950)
select (movie, movies directed by "george lucas")
"""

[
{"title": "shane", "year": 1953, "director": "george stevens", "genre": "western"]
...
]

"""
select
    yield
    generator
"""

"""
project (a relation, attributes...)
project (movie, director, year)
project (movie, year, genre, title)
"""

def select (r, up) :
    ...

def project (r, *a) :
    ...

"""
select
    yield
    yield comprehension
    generator comprehension
"""

"""
cross_join (a relation, another relation)
"""

"""
student table
sid, name, gpa, high school

college table
cid, name, location

apply table
sid, cid, major
"""

"""
cross_join
    yield comprehension
    generator comprehension
"""
