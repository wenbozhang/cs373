# -----------
# Fri,  6 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
big disconnect between lectures and projects
25+N quizzes, drop the lowest N
weekly blog
Piazza for class questions
Canvas for personal questions
Canvas for quizzes
HackerRank for exercises and tests
GitLab for source control
Docker for VM images
cold calling
CATME
"""

"""
who are you?
why are you here?
what's your experience in Web programming?
get some contact info
"""

"""
cs373 spring 2019 final entry
"""

"""
competitive programming
International Collegiate Programming Contest (ICPC), home is Baylor
around 40 years old
used to be part of the ACM
UT Programming Contests (UTPC)

12 students, 4 teams, to the regionals in early fall
usually at Baylor in Waco

11 regions in the country
135 regions in the world

our region has 25 schools in 3 states with 65 teams

2014: came in 2nd, Rice came in first, Thailand
2015: came in 2nd, Rice came in first, Morocco
2016: 1st, 2nd, 3rd, 4th, South Dakota
2017: 1st, Beijing
2018: 1st, Portugal
2019: if 1st, Moscow
"""

"""
writing tests first
to know if a test is good you a coverage tool
the tests have to be run often
continuous integration: automatically running a set of tests
in response to a push to a source control repository
"""

"""
Collatz Conjecture
around 100 years old

take a pos int
if even, divide by 2
otherwise, multiply by 3 and add 1
repeat until 1
"""

5 16 8 4 2 1

"""
cycle length of  5 is 6
cycle length of 10 is 7
"""

"""
/  is true  division, result is always a float
// is floor division, result is the type of the input
"""

"""
asserts are good for preconditions and postcondtions
asserts are not good for testing    (unit test framework)
asserts are not good for user input (exceptions)
"""

"""
what is "this" in Java
a reference to the object that a method is invoked
available in a non-static method
"""

"""
what is "self" in Python
same as "this" in Java
but, has to be introduced as the first argument
of a non-static method
and, can have any name
"""
