# -----------
# Mon, 17 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

""" ----------------------------------------------------------------------
What is the output of the following?
[Collatz]

a. 10
b. 11
c. 21
d. 22
"""

def f (n) :
    return n + (n >> 1) + 1

def test1 () :
    print(f(7))

"""
For odd n its computing (3n + 1) / 2.
(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
n + n/2 + 1, because n is odd
n + (n >> 1) + 1
"""

""" ----------------------------------------------------------------------
Given positive integers, b and e, let m = (e / 2) + 1.
If b < m, then max_cycle_length(b, e) = max_cycle_length(m, e)
[Collatz]

a. False
b. True
"""

"""
mcl(10, 100) = mcl(51, 100)
b = 10
e = 100
m = 51

mcl(200, 300)
b = 200
e = 300
m = 151

Consider b = 10, e = 100.
Then m = (100 / 2) + 1 = 51.
max_cycle_length(10, 100) = max_cycle_length(51, 100)
All the numbers in the range [10, 50] can be mapped to numbers in the
range [51, 100] by one or more doublings, so none of the numbers in the
range [10, 50] could have a larger cycle length than the numbers in the
range [51, 100].
"""

"""
excptions for user errors
let's start by pretending that we don't have exceptions
"""

# by the return
def f (...) :
    ...
    if (<something wrong>) :
        return <special value>
    ...

def g (...) :
    ...
    x = f(...)
    if (x == <special value>)
        <something wrong>

# use a global

h = 0

def f (...) :
    global h
    ...
    if (<something wrong>) :
        h = -1
        return ...
    ...

def g (...) :
    global h
    ...
    h = 0
    x = f(...)
    if (h == -1)
        <something wrong>

# use a parameter

def f (..., e2) :
    ...
    if (<something wrong>) :
        e2[0] = -1
        return ...
    ...

def g (...) :
    ...
    e = [0]
    x = f(..., e)
    if (e[0] == -1)
        <something wrong>

# in Java this doesn't work, because of pass by value

double f (..., int e2) :
    ...
    if (<something wrong>) :
        e2 = -1
        return ...
    ...

void g (...) :
    ...
    int e = 0
    double x = f(..., e)
    if (e == -1)
        <something wrong>

# in Java this doesn't work, either

double f (..., Integer e2) :
    ...
    if (<something wrong>) :
        e2 = -1; # creates a new Integer object
        return ...
    ...

void g (...) :
    ...
    Integer e = 0;
    double x = f(..., e)
    if (e == -1)
        <something wrong>

# in Java this does work

double f (..., int[] e2) :
    ...
    if (<something wrong>) :
        e2[0] = -1;
        return ...
    ...

void g (...) :
    ...
    int[] e = {0};
    double x = f(..., e)
    if (e[0] == -1)
        <something wrong>

# use exceptions

class Mammal (Exception) :
    ...

class Tiger (Mammal) :
    ...

def f (...) :
    ...
    if (<something wrong>) :
        raise Tiger(...)
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except Tiger as e : # put most specialized first
        ...
    except Mammal as e :
        ...
    except Exception as e :
        ...
    else : # only when no exception was raised
        ...
    finally : # always runs
        ...
    ...

"""
three scenarios when you raise
1. there is no try except block, go to the caller
2. there is a try except block, but it doesn't match, go the caller
3. there is a try except block, and it does match, run the except clause
"""

a = (2, 3)
b = (2, 3)
print(a is b) # false, comparing identity
print(a == b) # true,  comparing content

Integer i = 0;
Integer j = 0;
System.out.println(i == j);     # false, comparing identity
System.out.println(i.equals(j)) # true,  comparing content

"""
in Java   content comparison is achieved with the method equals()
in Python content comparison is achieved with the method __eq__()
"""

a = [2, 3]
print(type(a)) # list
print(len(a))  # 2

a = (2, 3)
print(type(a)) # tuple
print(len(a))  # 2

a = [2]
print(type(a)) # list
print(len(a))  # 1

a = (3)
print(type(a)) # int

a = (3,)
print(type(a)) # tuple
print(len(a))  # 1

a = []
print(type(a)) # list
print(len(a))  # 0

a = ()
print(type(a)) # tuple
print(len(a))  # 0

print(type(a) is tuple) # true
