# -----------
# Fri, 28 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

class map :
    def __init__ (self, ...)
        ...

    def __next__ (self)
        ...

    def __iter__ (self)
        ...

def f () :
    print("abc")
    yield 2
    print("def")
    yield 3
    print("ghi")

g = f()        # <nothing>
print(type(g)) # generator

print(iter(g) is g) # True

v = next(g) # abc
print(v)    # 2

v = next(g) # def
print(v)    # 3

v = next(g) # ghi, StopIteration

def map (uf, a) :
    ...

def map (bf, a, b) :
    ...

def map (tf, a, b, c) :
    ...

def f (x, y, t) :
    print([x, y, t])

f(2, 3, 4) # [2, 3, 4]
f(2)       # no

"""
three tokens
    =, *, **
two contexts
    function call, function args
"""

def f (x, y, *t) :
    assert(type(t) is tuple)
    print([x, y, t])

f(2)          # no
f(2, 3)       # [2, 3, ()]
f(2, 3, 4)    # [2, 3, (4,)]
f(2, 3, 4, 5) # [2, 3, (4, 5)]

def map (f, *a) :
    ...

class range :
    def __init__ (self, start, stop) :
        ...

    def __iter__ (self) :
        ...

    def __getitem__ (self, i) :
        ...
