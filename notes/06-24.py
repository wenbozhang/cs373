# -----------
# Mon, 24 Jun
# -----------

"""
Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm, GDC 6.302
        T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924
    Randall
        T,   2-3pm,     GDC 1.302
    Chase
        Th, 12-1pm,     GDC 3.302

Piazza
    ask and answer questions
    please be proactive
"""

"""
all team members should be on Slack and Slack should be on your phone

BE RESPONSIVE

try Zoom for virtual meetings, 40 min for free

ASAP provide 5 user stories to development team on their GitLab issue tracker
you probably have to invite customer team to your GitLab issue tracker

first phase, no DB needed at all
the only thing that needs to be automated is the GitLab stats for your About page
"""
