#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = global-statement
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# --------------------
# DecoratorPatternT.py
# --------------------

from typing   import Dict
from unittest import main, TestCase

d = {} # type: Dict[int, int]

def cache_function (f) :
    def g (n) :
        if n not in d :
            d[n] = f(n)
        return d[n]
    return g

class cache_class :
    def __init__ (self, f) :
        self.f = f

    def __call__ (self, n) :
        if n not in d :
            d[n] = self.f(n)
        return d[n]

@cache_function
def fibonacci (n) :
    if n < 2 :
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)

class MyUnitTests (TestCase) :
    def setUp (self) :
        global d
        d = {}

    def test0 (self) :
        self.assertEqual(fibonacci(0), 0)
        self.assertEqual(d, {0: 0})

    def test1 (self) :
        self.assertEqual(fibonacci(1), 1)
        self.assertEqual(d, {1: 1})

    def test2 (self) :
        self.assertEqual(fibonacci(2), 1)
        self.assertEqual(d, {0: 0, 1: 1, 2: 1})

    def test3 (self) :
        self.assertEqual(fibonacci(3), 2)
        self.assertEqual(d, {0: 0, 1: 1, 2: 1, 3: 2})

    def test4 (self) :
        self.assertEqual(fibonacci(4), 3)
        self.assertEqual(d, {0: 0, 1: 1, 2: 1, 3: 2, 4: 3})

    def test5 (self) :
        self.assertEqual(fibonacci(5), 5)
        self.assertEqual(d, {0: 0, 1: 1, 2: 1, 3: 2, 4: 3, 5: 5})

if __name__ == "__main__" : # pragma: no cover
    main()
