#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------------------
# DecoratorPattern.py
# -------------------

@cache
def fibonacci (n) :
    if n < 2 :
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)

def test0 () :
    assert fibonacci(0) == 0
    assert d == {0: 0}

def test1 () :
    assert fibonacci(1) == 1
    assert d == {1: 1}

def test2 () :
    assert fibonacci(2) == 1
    assert d == {0: 0, 1: 1, 2: 1}

def test3 () :
    assert fibonacci(3) == 2
    assert d == {0: 0, 1: 1, 2: 1, 3: 2}

def test4 () :
    assert fibonacci(4) == 3
    assert d == {0: 0, 1: 1, 2: 1, 3: 2, 4: 3}

def test5 () :
    assert d == {}
    assert fibonacci(5) == 5
    assert d == {0: 0, 1: 1, 2: 1, 3: 2, 4: 3, 5: 5}

def main () :
    print("DecoratorPattern.py")
    n = int(input())
    eval("test" + str(n) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
