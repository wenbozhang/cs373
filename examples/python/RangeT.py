#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = redundant-unittest-assert
# pylint: disable = too-few-public-methods

# ---------
# RangeT.py
# ---------

# https://docs.python.org/3/library/functions.html#func-range

from typing   import Iterable, Iterator
from unittest import main, TestCase

class my_range_1 (Iterable[int]) :
    class iterator (Iterator[int]) :
        def __init__ (self, b: int, e: int) -> None :
            self.b = b
            self.e = e

        def __iter__ (self) -> Iterator[int] :
            return self

        def __next__ (self) -> int :
            if self.b == self.e :
                raise StopIteration()
            v = self.b
            self.b += 1
            return v

    def __init__ (self, b: int, e: int) -> None :
        self.b = b
        self.e = e

    def __getitem__ (self, i) :
        if i < 0 :
            if not (self.b - self.e) <= i <= -1 :
                raise IndexError("range object index out of range")
            return self.e + i
        if not 0 <= i < (self.e - self.b) :
            raise IndexError("range object index out of range")
        return self.b + i

    def __iter__ (self) -> Iterator[int] :
        return my_range_1.iterator(self.b, self.e)

class my_range_2 (Iterable[int]) :
    def __init__ (self, b: int, e: int) -> None :
        self.b = b
        self.e = e

    def __getitem__ (self, i) :
        if i < 0 :
            if not (self.b - self.e) <= i <= -1 :
                raise IndexError("range object index out of range")
            return self.e + i
        if not 0 <= i < (self.e - self.b) :
            raise IndexError("range object index out of range")
        return self.b + i

    @staticmethod
    def iterator (b, e) :
        while b != e :
            yield b
            b += 1

    def __iter__ (self) -> Iterator[int] :
        return my_range_2.iterator(self.b, self.e)

class my_range_3 (Iterable[int]) :
    def __init__ (self, b: int, e: int) -> None :
        self.b = b
        self.e = e

    def __getitem__ (self, i) :
        if i < 0 :
            if not (self.b - self.e) <= i <= -1 :
                raise IndexError("range object index out of range")
            return self.e + i
        if not 0 <= i < (self.e - self.b) :
            raise IndexError("range object index out of range")
        return self.b + i

    def __iter__ (self) -> Iterator[int] :
        b = self.b
        while b != self.e :
            yield b
            b += 1

class MyUnitTests (TestCase) :
    def setUp (self) :
        self.a = [
            my_range_1,
            my_range_2,
            my_range_3,
            range]

    def test0 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                x = f(2, 2)
                x = range(2, 2)
                self.assertFalse(hasattr(x, "__next__"))
                self.assertTrue (hasattr(x, "__iter__"))
                self.assertTrue (hasattr(x, "__getitem__"))
                p = iter(x)
                self.assertTrue (hasattr(p, "__next__"))
                self.assertTrue (hasattr(p, "__iter__"))
                self.assertFalse(hasattr(p, "__getitem__"))
                self.assertIs(iter(p), p)
                self.assertEqual(list(x), [])

    def test1 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                x = f(2, 3)
                self.assertEqual(list(x), [2])
                self.assertEqual(list(x), [2])

    def test2 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                x = f(2, 4)
                self.assertEqual(list(x), [2, 3])
                self.assertEqual(list(x), [2, 3])

    def test3 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                x = f(2, 5)
                self.assertEqual(list(x), [2, 3, 4])
                self.assertEqual(list(x), [2, 3, 4])

    def test4 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                x = f(2, 5)
                self.assertEqual(x[0],  2)
                self.assertEqual(x[1],  3)
                self.assertEqual(x[2],  4)
                self.assertEqual(x[-1], 4)
                try :
                    self.assertEqual(x[3], 0)
                    self.assertTrue(False)
                except IndexError as e :
                    assert e.args == ('range object index out of range',)
                try :
                    self.assertEqual(x[-4], 0)
                    self.assertTrue(False)
                except IndexError as e :
                    assert e.args == ('range object index out of range',)

if __name__ == "__main__" : # pragma: no cover
    main()
